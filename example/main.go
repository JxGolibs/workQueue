package main

import (
	"fmt"
	"runtime"
	"time"

	"gitee.com/JxGolibs/workQueue"
)

//定义一个实现Job接口的数据
type Score struct {
	Num int
}

//定义对数据的处理
func (s *Score) Do() {
	fmt.Println("num:", s.Num)
	//	time.Sleep(500 * time.Millisecond) //模拟执行的耗时任务
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	startTime := time.Now()
	//
	workPool := workQueue.NewWorkerPool(1000, 10000)
	workPool.Run()

	go func() {
		for i := 1; i <= 1000000; i++ {
			//数据传进去会被自动执行Do()方法，具体对数据的处理自己在Do()方法中定义
			sc := &Score{Num: i}
			workPool.Add(sc)
		}
		fmt.Println("add end ============================================")
	}()

	//////////////

	for {
		fmt.Println("runtime.NumGoroutine() :", runtime.NumGoroutine(), time.Since(startTime))
		time.Sleep(5 * time.Second)
	}

}
