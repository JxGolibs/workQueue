package workQueue

import "time"

// --------------------------- Job ---------------------
type Job interface {
	Do()
}
type jobQueueChan chan Job

type State struct {
	Timestamp  int64
	QueueCount int //当前剩余队列数量
}

// --------------------------- Worker ---------------------
type worker struct {
	JobChan jobQueueChan //每一个worker对象具有JobQueue（队列）属性。
}

func NewWorker() worker {
	return worker{JobChan: make(chan Job)}
}

//启动参与程序运行的Go程数量
func (w worker) Run(wq chan jobQueueChan) {
	go func() {
		for {

			wq <- w.JobChan //处理任务的Go程队列数量有限，每运行1个，向队列中添加1个，队列剩余数量少1个 (JobChain入队列)
			select {
			case job := <-w.JobChan:
				//fmt.Println("xxx2:",w.JobChan)

				go job.Do() //执行操作
			}
		}
	}()
}

// --------------------------- WorkerPool ---------------------
type WorkerPool struct { //线程池：
	Workerlen   int               //线程池的大小
	jobQueue    jobQueueChan      //Job队列，接收外部的数据
	WorkerQueue chan jobQueueChan //worker队列：处理任务的Go程队列
}

func NewWorkerPool(workerlen int, maxQueue int) *WorkerPool {
	return &WorkerPool{
		Workerlen:   workerlen,
		jobQueue:    make(jobQueueChan, maxQueue),
		WorkerQueue: make(chan jobQueueChan, workerlen),
	}
}

func (wp *WorkerPool) Add(job Job) {
	//fmt.Println(len(wp.jobQueue))
	wp.jobQueue <- job
}

func (wp *WorkerPool) Run() {
	// fmt.Println("初始化worker")
	//初始化worker(多个Go程)
	for i := 0; i < wp.Workerlen; i++ {
		worker := NewWorker()
		worker.Run(wp.WorkerQueue) //开启每一个Go程
	}
	// 循环获取可用的worker,往worker中写job
	go func() {
		for {
			// fmt.Println("jobQueue", len(wp.jobQueue))
			select {
			//将JobQueue中的数据存入WorkerQueue
			case job := <-wp.jobQueue: //线程池中有需要待处理的任务(数据来自于请求的任务) :读取JobQueue中的内容
				worker := <-wp.WorkerQueue //队列中有空闲的Go程   ：读取WorkerQueue中的内容,类型为：JobQueue
				worker <- job              //空闲的Go程执行任务  ：整个job入队列（channel） 类型为：传递的参数（Score结构体）
				//	fmt.Println("jobQueue1", len(wp.jobQueue))
			}

		}
	}()
}

func (wp *WorkerPool) State() State {
	return State{
		Timestamp:  time.Now().Unix(),
		QueueCount: len(wp.jobQueue),
	}
}
